<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'varetreat');

/** MySQL database username */
define('DB_USER', 'varetreat');

/** MySQL database password */
define('DB_PASSWORD', 'varetreat123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '6}+JWhsdXNlZQA.uokndc~TV``c2+ oCjP^ckD)sHR1=D)G ^=SokTo9d[Oy dud');
define('SECURE_AUTH_KEY',  'Pm9pcD+5z-aKQxA0)9tj2AKpy3|U#s+eq/jB!TTC20@2J7F+Z2&q|- +dBw-m8?z');
define('LOGGED_IN_KEY',    'nl$6#nG69M,7A] ]?)Y}RC|r]7ycgjU.>[-F?(~S,Iq+(&]1T|pewz<H(u<~,G2~');
define('NONCE_KEY',        '9vEsq*3;3[{X} ,&-OaR;KGK%40;%HcQ25b|8}~mg[i>tqASIwXU1LP&pmB[7z&_');
define('AUTH_SALT',        'j%IXg=5XJ93(gTJYoc[sMNH}V@=QbboK@1,!I4.x+Z2mE{WwhiyRdPzp.UJ9 eCL');
define('SECURE_AUTH_SALT', 'EFH|kzQ&-.`6^mLa-k^V7q>b`*hz-V53xG#wj._f|;01t*RL4)iqt**7,Q3qW&TX');
define('LOGGED_IN_SALT',   'z_hlq-J8Z|Xur#XCCRF(fv:UVKQ^}<<t!(rLr-lxB8n]bjHIO>p{7TOPjqt>/om&');
define('NONCE_SALT',       '{Kh]VEpc&heBI|,NoNq!n-7~9%-hW}u`2Skxiw0anDy@W,p[?yin< I>CYrO0cj$');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
