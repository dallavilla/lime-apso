<!--
<div class="grid_3" id="sidebar">
	<h3>History Adventures</h3>
	<img style="margin: 5px 0" src="<?php bloginfo('template_directory'); ?>/images/history_adventures.jpg" class="shadow" alt="Experience History" />
	<p>From small towns to farmland, hidden coves to courthouses, this region is rich with important history. Journey through the past in the very places where people and events shaped our nation.<br /><a href="<?php echo get_category_link(6); ?>" class="more">Click for more info</a></p>
	<h3>Outdoor Adventures</h3>
	<img style="margin: 5px 0" src="<?php bloginfo('template_directory'); ?>/images/outdoor_adventures.jpg" class="shadow" alt="Get Outdoors" />
	<p>Where small towns cozy up to acres of beautiful park land, there's an easy path to the great outdoors. Find a natural state of happiness along the trails, waterways, forests and lakes here.<br /><a href="<?php echo get_category_link(7); ?>" class="more">Click for more info</a></p>
</div>
-->

<div class="grid_3">
	<ul id="sidebar">
		<?php if ( !function_exists('dynamic_sidebar')
		        || !dynamic_sidebar() ) : ?>
		<li id="history_side">
			<h3>History Adventures</h3>
			<img style="margin: 5px 0" src="<?php bloginfo('template_directory'); ?>/images/history_adventures.jpg" class="shadow" alt="Experience History" />
			<p>From small towns to farmland, hidden coves to courthouses, this region is rich with important history. Journey through the past in the very places where people and events shaped our nation.<br /><a href="<?php echo get_category_link(6); ?>" class="more">Click for more info</a></p>
		</li>
		<li id="outdoor_side">
			<h3>Outdoor Adventures</h3>
			<img style="margin: 5px 0" src="<?php bloginfo('template_directory'); ?>/images/outdoor_adventures.jpg" class="shadow" alt="Get Outdoors" />
			<p>Where small towns cozy up to acres of beautiful park land, there's an easy path to the great outdoors. Find a natural state of happiness along the trails, waterways, forests and lakes here.<br /><a href="<?php echo get_category_link(7); ?>" class="more">Click for more info</a></p>
		</li>
		<?php endif; ?>
	</ul>
</div>

