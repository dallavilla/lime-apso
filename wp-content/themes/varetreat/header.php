<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head>
<title><?php bloginfo('name'); ?><?php if ( is_single() ) { ?> &raquo; News Archive <?php } ?><?php wp_title(' &raquo; '); ?></title>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
<link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet" type="text/css" media="screen" />
<link href="<?php bloginfo('stylesheet_directory'); ?>/960.css" rel="stylesheet" type="text/css" media="screen" />
<link href="<?php bloginfo('stylesheet_directory'); ?>/print.css" rel="stylesheet" type="text/css" media="print" />
<!--[if IE]>
	<link href="<?php bloginfo('stylesheet_directory'); ?>/ie.css" rel="stylesheet" type="text/css" media="screen" />
<![endif]-->
<link rel="shortcut icon" href="/favicon.ico" />
<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/ui.js" type="text/javascript"></script>

<?php wp_head(); ?>

<script src="<?php bloginfo('template_directory'); ?>/js/jquery.pagination.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.cycle.all.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/slides.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8" src="<?php bloginfo('template_directory'); ?>/js/calendar.js"></script>
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/js/calendar.css" type="text/css" media="screen" charset="utf-8" />

<script type="text/javascript" src="http://maps.google.com/maps/api/js?libraries=geometry&amp;sensor=true"></script>


<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23023953-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>
<body>
<!--
<script type="text/javascript" language="javascript">
$(document).ready(function(){
		bgImageTotal=1;
		randomNumber = Math.round(Math.random()*(bgImageTotal-1))+1;
		imgPath=('<?php bloginfo('template_directory'); ?>/images/backgrounds/'+randomNumber+'.jpg');
		$('#homecontainer').css('background-image', ('url("'+imgPath+'")'));
	});
</script>
-->

<?php if (is_page('travel-guides')) { ?>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.validate.js"></script>
<script type="text/javascript">
$(document).ready(function(){ $("#submit").validate(); });
$(document).ready(function(){
	$("form#submit").submit(function() {
		$sendok = $("#submit").valid();
		if ($sendok == true){
			var data = $(this).serialize();
			$.post('<?php bloginfo('template_directory'); ?>/request_form.php', data, function(results) {
				$('form#submit').hide(function(){$('div.success').fadeIn();});
			});	
			return false;
		}
	});
});
</script>
<?php } ?>
<div id="printlogo"><img src="<?php bloginfo('template_directory'); ?>/images/vrlogo_print.jpg" /></div>
<div id="header">
	<div class="container_12">
		<div class="grid_12">
			<div class="logo"><a href="<?php echo get_option('home'); ?>"><h1><?php bloginfo('name'); ?></h1></a></div>
			<a id="contactUs" href="<?php echo get_page_link(10); ?>">Contact Us</a>
			<a id="requestGuide" href="<?php echo get_page_link(463); ?>">Request a Travel Guide</a>
			
			<div id="nav">
				<ul>
					<?php
						//wp_list_categories('orderby=ID&depth=1&include=3,8,9&title_li=&hide_empty=0&use_desc_for_title=0'); 
						//wp_list_categories('orderby=ID&depth=1&include=4&title_li=&hide_empty=0&use_desc_for_title=0');
					?>
					

					<li class="cat-item cat-item-3<? if(strpos($_SERVER["REQUEST_URI"], "/about-the-region/")!==false){ print " current-cat"; } ?>"><a href="http://varetreat.bcfdev2.com/category/about-the-region/" title="View all posts filed under About The Region">About The Region</a></li>
					<li class="cat-item cat-item-8<? if(strpos($_SERVER["REQUEST_URI"], "/what-to-do/")!==false){ print " current-cat"; } ?>"><a href="http://varetreat.bcfdev2.com/category/what-to-do/" title="View all posts filed under What to Do">What to Do</a></li>
					<li class="cat-item cat-item-9<? if(strpos($_SERVER["REQUEST_URI"], "/where-to-stay/")!==false){ print " current-cat"; } ?>"><a href="http://varetreat.bcfdev2.com/category/where-to-stay/" title="View all posts filed under Where to Stay">Where to Stay</a></li>
					<li class="cat-item cat-item-4<? if(strpos($_SERVER["REQUEST_URI"], "/adventures/")!==false){ print " current-cat"; } ?>"><a href="http://varetreat.bcfdev2.com/category/adventures/" title="View all posts filed under Adventures">Adventures</a></li>
					
					<li><?php get_search_form(); ?></li>
				</ul>
			</div>
		</div>
	</div>
</div>

<div id="mapbg">