<?php 
// Disable WP jQuery
if ( function_exists( 'add_theme_support' ) ) { 
  add_theme_support( 'post-thumbnails' ); 
}

if( !is_admin()){
       wp_deregister_script('jquery');
       wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"), false, '1.5.1');
       wp_enqueue_script('jquery');
}

/* i forget */
add_filter('single_template', create_function('$t', 'foreach( (array) get_the_category() as $cat ) { if ( file_exists(TEMPLATEPATH . "/single-{$cat->term_id}.php") ) return TEMPLATEPATH . "/single-{$cat->term_id}.php"; } return $t;' ));

/* sets length of the excerpt */
function new_excerpt_length($length) {
	return 30;
}
add_filter('excerpt_length', 'new_excerpt_length');

function addthis(){
	
	print '<!-- AddThis Button BEGIN -->
	<div class="addthis_toolbox addthis_default_style ">
	<a class="addthis_button_preferred_1"></a>
	<a class="addthis_button_preferred_2"></a>
	<a class="addthis_button_preferred_3"></a>
	<a class="addthis_button_preferred_4"></a>
	<a class="addthis_button_compact"></a>
	<a class="addthis_counter addthis_bubble_style"></a>
	</div>
	<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4db9bf042b767631"></script>
	<!-- AddThis Button END -->';
	
}

/* breadcrumbs */
function dimox_breadcrumbs() {
 
  $delimiter = '&raquo;';
  $home = 'Home'; // text for the 'Home' link
  $before = '<span class="current">'; // tag before the current crumb
  $after = '</span>'; // tag after the current crumb
 
  if ( !is_home() && !is_front_page() || is_paged() ) {
 
    echo '<div id="crumbs">';
 
    global $post;
    $homeLink = get_bloginfo('url');
    echo '<a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';
 
    if ( is_category() ) {
      global $wp_query;
      $cat_obj = $wp_query->get_queried_object();
      $thisCat = $cat_obj->term_id;
      $thisCat = get_category($thisCat);
      $parentCat = get_category($thisCat->parent);
      if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
      echo $before . single_cat_title('', false) . $after;
 
    } elseif ( is_day() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('d') . $after;
 
    } elseif ( is_month() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('F') . $after;
 
    } elseif ( is_year() ) {
      echo $before . get_the_time('Y') . $after;
 
    } elseif ( is_single() && !is_attachment() ) {
      if ( get_post_type() != 'post' ) {
        $post_type = get_post_type_object(get_post_type());
        $slug = $post_type->rewrite;
        echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a> ' . $delimiter . ' ';
        echo $before . get_the_title() . $after;
      } else {
        $cat = get_the_category(); $cat = $cat[0];
        echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
        echo $before . get_the_title() . $after;
      }
 
    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' ) {
      $post_type = get_post_type_object(get_post_type());
      echo $before . $post_type->labels->singular_name . $after;
 
    } elseif ( is_attachment() ) {
      $parent = get_post($post->post_parent);
      $cat = get_the_category($parent->ID); $cat = $cat[0];
      echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
      echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a> ' . $delimiter . ' ';
      echo $before . get_the_title() . $after;
 
    } elseif ( is_page() && !$post->post_parent ) {
      echo $before . get_the_title() . $after;
 
    } elseif ( is_page() && $post->post_parent ) {
      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
      echo $before . get_the_title() . $after;
 
    } elseif ( is_search() ) {
      echo $before . 'Search results for "' . get_search_query() . '"' . $after;
 
    } elseif ( is_tag() ) {
      echo $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;
 
    } elseif ( is_author() ) {
       global $author;
      $userdata = get_userdata($author);
      echo $before . 'Articles posted by ' . $userdata->display_name . $after;
 
    } elseif ( is_404() ) {
      echo $before . 'Error 404' . $after;
    }
 
    if ( get_query_var('paged') ) {
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
      echo __('Page') . ' ' . get_query_var('paged');
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    }
 
    echo '</div>';
 
  }
} // end dimox_breadcrumbs()

/* XML Parser */

include_once("simple_html_dom.php");

function varetreat_xml_single_page(){
	global $blog_id;
	global $wpdb;

	$currpage = "http://" . $_SERVER["HTTP_HOST"] . $_SERVER['REQUEST_URI'];
	if(!is_admin() && strpos($currpage, get_option('siteurl')."/listing/")!==false){
		apply_filters( 'page_template', varetreat_listing($template) );
	}
	
}
add_action("init", "varetreat_xml_single_page", 1);

function varetreat_listing($t){
	global $blog_id;
	global $wpdb;





	$tmp = split("/listing/", $_SERVER['REQUEST_URI']);
	$key = split("/", $tmp[1]);
	$id = $key[0];		
        
		/*
		$tmp = split("/listing/", $_SERVER['REQUEST_URI']);
		$key = split("/", $tmp[1]);
		$id = $key[0];
		
		if($_GET["f"]!="" && $_GET["f"]!=null){
			$feed_url = "http://www.virginia.org/events/xml/VARetreat/" . $_GET["f"] . ".xml";
		} else {
			if($_COOKIE["xml_file"]!="" && $_COOKIE["xml_file"]!=null){
				$feed_url = $_COOKIE["xml_file"];
			} else {
				$feed_url = "http://www.virginia.org/events/xml/VARetreat/locAttrVARTRT.xml";
			}
		}


		$opts = array('http' => array('user_agent' => 'PHP libxml agent'));

		$context = stream_context_create($opts);
		libxml_set_streams_context($context);

		$doc = new DOMDocument();
		$doc = DOMDocument::load($feed_url);

		// $elements = $doc->getElementsByTagName("item"); 
		
		$xpath = new DOMXpath($doc);
		$elements = $xpath->query("item[attrID='" . $id . "'][position() < 2]");
		
		foreach ($elements as $element) {
			include_once($_SERVER["DOCUMENT_ROOT"] . '/wp-content/themes/varetreat/listing.php');
		}
;
		
		*/
		
		get_header();
		
			$response = $wpdb->get_results( "SELECT * FROM varetreat_listings WHERE xml LIKE '%<attrid>" . intval($id) . "</attrid>%' LIMIT 1"  );
			
			foreach( $response as $item ){

				// print $item->xml . "<hr/>";

				$doc = new DOMDocument();
				$doc = DOMDocument::loadXML(trim("<item>" . $item->xml . "</item>"));
				
				// $xml = str_get_html($item->xml);
				
				$xpath = new DOMXpath($doc);
				$elements = $xpath->query("//item");
				
				foreach ($elements as $element) {
					include_once($_SERVER["DOCUMENT_ROOT"] . '/wp-content/themes/varetreat/listing.php');
				}
				
			}
		
		
		get_footer();
		exit;
		
		
		
		
	
}

function create_dropdown($omgvar){

	include("var_categories.php");
	
	$opts = "";
	$retval = "";
	switch($omgvar){
		
		case "todo_city":
			$opts = explode(",", $attr_city . $dining_city . $shopping_city . $events_city);
			$opts = array_unique($opts);
			$typ = "city";
			break;
		case "todo_locality":
			$opts = explode(",", $attr_locality . $dining_locality . $shopping_locality . $events_locality);
			$opts = array_unique($opts);
			$typ = "locality";
			break;
			
			
		
		case "attr_city":
			$opts = explode(",", $attr_city);
			$typ = "city";
			break;
			
		case "attr_locality" :
			$opts = explode(",", $attr_locality);
			$typ = "locality";
			break;

		case "dining_city":
			$opts = explode(",", $dining_city);
			$typ = "city";
			break;
			
		case "dining_locality" :
			$opts = explode(",", $dining_locality);
			$typ = "locality";
			break;
			
		case "shopping_city":
			$opts = explode(",", $shopping_city);
			$typ = "city";
			break;

		case "shopping_locality" :
			$opts = explode(",", $shopping_locality);
			$typ = "locality";
			break;

		case "events_city":
			$opts = explode(",", $events_city);
			$typ = "city";
			break;

		case "events_locality":
			$opts = explode(",", $events_locality);
			$typ = "locality";
			break;

		case "lodging_city":
			$opts = explode(",", $lodging_city);
			$typ = "city";
			break;

		case "lodging_locality":
			$opts = explode(",", $lodging_locality);
			$typ = "locality";
			break;
			
		case "lodging_type":
			$opts = explode(",", $lodging_type);
			$typ = "cat";
			break;
			
	}
	
	asort($opts);
	
	foreach($opts as $z){
		if($z == ""){
			$retval .= "<option value=\"\">" . ucwords($typ) . "</option>";
		} else {
			$retval .= "<option value=\"$z\">" . ucwords($z) . "</option>";
		}
	}
	
	return "<select name=\"$typ\" class=\"dd-" . $typ . "\" onchange=\"this.form.submit();\">$retval</select>";
}

/* ERIK USE THE FUNCTION BELOW */


function varetreat_events($url, $num, $textlen){

	// print "BLAM!";

	$z = 1; 

	$output .= "<ul class=\"zebra\" style=\"padding-top: 15px;\">";
	
	global $wpdb;

	// print "SELECT * FROM varetreat_listings WHERE maincategory = 'locEventsVARTRT' and end >= '" . date("Y-m-d") . "' ORDER BY start ASC LIMIT " . intval($num);
	
	$response = $wpdb->get_results( "SELECT * FROM varetreat_listings WHERE maincategory = 'locEventsVARTRT' and end >= '" . date("Y-m-d") . "' ORDER BY start ASC LIMIT " . intval($num) );
	
	// $wpdb->prepare()
	
	
	
	foreach ($response as $item){

		
		$xml = str_get_html($item->xml);

		$lat = $wpdb->get_var( "SELECT lat FROM varetreat_listings_latlon_cache WHERE aid = 'a" . $xml->find("attrid", 0)->innertext . "' LIMIT 1" );
		$lon = $wpdb->get_var( "SELECT lon FROM varetreat_listings_latlon_cache WHERE aid = 'a" . $xml->find("attrid", 0)->innertext . "' LIMIT 1" );
		
		if($z==1){ $output .= "<li class=\"odd\" rel=\"$lat,$lon\" id=\"a" . $xml->find("attrid", 0)->innertext . "\">"; } else { $output .= "<li rel=\"$lat,$lon\" id=\"a" . $xml->find("attrid", 0)->innertext . "\">"; }
		
		$output .=  "<h3><b>" . date("m/d/Y", strtotime($item->start)) . " - " . date("m/d/Y", strtotime($item->end)) . "</b><br/><a href='/listing/" . $xml->find("attrid", 0)->innertext . "/'>" . str_replace("&apos;", "&#39;", $item->title) . "</a></h3>";

		$output .= "</li>";
		
		$z *= -1;
		
		$xml->clear();
	}

	$output = "$output</ul>";


	
	return $output;
	
}


function varetreat_xml($url, $num, $textlen){


	$z = 1; 


	if(strpos($_SERVER["REQUEST_URI"], "/events/")!==false){
		$valid_date = " onsubmit=\"return checkDates();\"";
	} else {
		$valid_date = "";
	}


	$output .= "<form action=\"$_SERVER[REQUEST_URI]\" method=\"post\" class='search-listings-form' $valid_date ><h2>Filter By:</h2>";

	if(strpos($_SERVER["REQUEST_URI"], "/attractions/")!==false){
		$output .= create_dropdown("attr_city");
		$output .= create_dropdown("attr_locality");
		$maincat = "locAttrVARTRT";
		$orderby = "title ASC";
	}
	
	if(strpos($_SERVER["REQUEST_URI"], "/dining/")!==false){
		$output .= create_dropdown("dining_city");
		$output .= create_dropdown("dining_locality");
		$maincat = "locDiningVARTRT";
		$orderby = "title ASC";
	}
	
	if(strpos($_SERVER["REQUEST_URI"], "/shopping/")!==false){
		$output .= create_dropdown("shopping_city");
		$output .= create_dropdown("shopping_locality");
		$maincat = "locShoppingVARTRT";
		$orderby = "title ASC";
	}
	
	if(strpos($_SERVER["REQUEST_URI"], "/events/")!==false){
		$output .= create_dropdown("events_city");
		$output .= create_dropdown("events_locality");
		$output .= "<div style=\"padding: 5px 5px 5px 30px\">
			Start Date <input type=\"text\" name=\"sdate\" value=\"" . date("m/d/Y") . "\" id=\"sdate\" onclick=\"showCalendarControl(this, document.getElementById('edate').value, 0);\"/> &nbsp; &nbsp; 
			End Date <input type=\"text\" name=\"edate\" value=\"" . date("m/d/Y", strtotime("+1 year")) . "\" id=\"edate\" onclick=\"showCalendarControl(this, document.getElementById('sdate').value, 1);\" />
			<input type=\"submit\" name=\"some_name\" value=\"Search\" id=\"some_name\">
			&nbsp; <input type=\"button\" name=\"show_all\" value=\"Show All Events\" onclick=\"window.location.href='" . $_SERVER["REQUEST_URI"] . "';\"></div>";
		
		$maincat = "locEventsVARTRT";
		$orderby = " start ASC";
	}

	$output .= "</form>";
	
	if($_SERVER["REQUEST_METHOD"] == "POST" ){
		$output .= "<div class='searched-for'>You searched for: " . ucwords($_POST["city"] . $_POST["locality"]);
		
		if(trim(ucwords($_POST["city"] . $_POST["locality"])) !="" ){
			$output .= ",";
		}
		
		if(trim($_POST["sdate"])!="" && trim($_POST["edate"])!=""){
			$output .= " $_POST[sdate] to $_POST[edate]";
		}
		
		$output .= "</div>";
	}
	
	$output .= "<div class=\"paginations\"></div><ul class=\"zebra\">";
	
	//print create_dropdown("lodging_type");
	//print create_dropdown("lodging_city");
	/* ********************************* */
	/*
	// OLD AND BUSTED.
	$output = "";
	$xml = file_get_html($url);
	$count = 0;
	setcookie("xml_file", $url, time()+3600, "/");
	print "<hr/>";
	
	foreach($xml->find("item") as $item){
		
		$count++;
		
		if($z==1){ $output .= "<li class=\"odd\">"; } else { $output .= "<li>"; }

		if(trim($item->find("daterange",0)->innertext)!=""){
			$output .= "<p class='date'>" . $item->find("daterange",0)->innertext . "</p>"; 
		}


		$output .= "
						<h3>
							<a href=\"/listing/" . $item->find("attrID",0)->innertext . "/\">" . $item->find("title",0)->innertext . "</a>
						</h3>"; 
						

						
		$output .= "<b>" . $item->find("locality",0)->innertext . "</b><br/>"; 
						
		if($textlen == "" || $textlen == "-1"){
			$output .= strip_tags(html_entity_decode($item->find("description",0)->innertext));
		} else {
			$output .= substr(strip_tags(html_entity_decode($item->find("description",0)->innertext)), 0, intval($textlen)) . "&hellip;";
		}
		
						
		$output .= "</li>\n";
		$z*=-1;
		
		if(intval($num)!=-1 && $count > intval($num)){ break; }
		
	}
	
	if($output!=""){ $output = "<ul class=\"zebra\">" . $output . "</ul>"; }
	*/


	// NEW HAWTNESS

	global $wpdb;
	
	if($_SERVER["REQUEST_METHOD"] == "POST" ){
		
		if( $_POST["city"] !="" ){ $query_city = " and xml LIKE '%<citytown>" . mysql_real_escape_string($_POST["city"]) . "</citytown>%'"; }
		if( $_POST["locality"] !="" ){ $query_locality = " and xml LIKE '%<locality>" . mysql_real_escape_string($_POST["locality"]) . "</locality>%'"; }
		
		if( $_POST["sdate"] !="" && $_POST["edate"]!=""){
			$query_dates = " and ( (start >='" . date("Y-m-d", strtotime(mysql_real_escape_string($_POST["sdate"])))  . "' and start <= '" . date("Y-m-d", strtotime(mysql_real_escape_string($_POST["edate"]))) . "' ) or (  end >='" . date("Y-m-d", strtotime(mysql_real_escape_string($_POST["sdate"])))  . "' and end <= '" . date("Y-m-d", strtotime(mysql_real_escape_string($_POST["edate"]))) . "'  )  )";
			//$query_dates = " and ( (start <= '" . date("Y-m-d", strtotime(mysql_real_escape_string($_POST["edate"]))) . "' ) or (  end >='" . date("Y-m-d", strtotime(mysql_real_escape_string($_POST["edate"])))  . "' and end <= '" . date("Y-m-d", strtotime(mysql_real_escape_string($_POST["edate"]))) . "'  )  )";
		} 
		

		$response = $wpdb->get_results( "SELECT * FROM varetreat_listings WHERE maincategory = '$maincat' $query_city $query_locality $query_dates ORDER BY $orderby"  );
	} else {
		$response = $wpdb->get_results( "SELECT * FROM varetreat_listings WHERE maincategory = '$maincat' ORDER BY $orderby" );
	}
	
	// $wpdb->prepare()
	
	
	
	foreach ($response as $item){

		
		$xml = str_get_html($item->xml);

		//print "SELECT lat FROM varetreat_listings_latlon_cache WHERE aid = 'a" . $xml->find("attrid", 0)->innertext . "' ORDER BY id DESC LIMIT 1<br/>";

		$lat = $wpdb->get_var( "SELECT lat FROM varetreat_listings_latlon_cache WHERE aid = 'a" . $xml->find("attrid", 0)->innertext . "' ORDER BY id DESC LIMIT 1" );
		$lon = $wpdb->get_var( "SELECT lon FROM varetreat_listings_latlon_cache WHERE aid = 'a" . $xml->find("attrid", 0)->innertext . "' ORDER BY id DESC LIMIT 1" );
		
		if($z==1){ $output .= "<li class=\"odd\" rel=\"$lat,$lon\" id=\"a" . $xml->find("attrid", 0)->innertext . "\">"; } else { $output .= "<li rel=\"$lat,$lon\" id=\"a" . $xml->find("attrid", 0)->innertext . "\">"; }

		$output .= "<div class='marker'></div><div class='item-content'>";
		
		if(strpos($_SERVER["REQUEST_URI"], "/events/")){
			$output .= "<b>" . date("m/d/Y", strtotime($item->start)) . " - " . date("m/d/Y", strtotime($item->end)) . "</b>";
		}
		
		$output .=  "
						<h3><a href='/listing/" . $xml->find("attrid", 0)->innertext . "/'>" . str_replace("&apos;", "&#39;", $item->title) . "</a></h3>
						<b>" . $xml->find("locality", 0)->innertext . "</b>";
		$output .= "<p>";

		if($textlen == "" || $textlen == "-1"){
			$output .= str_replace("&apos;", "&#39;",strip_tags(html_entity_decode($xml->find("description", 0)->innertext)));
		} else {
			$output .= str_replace("&apos;", "&#39;",substr(strip_tags(html_entity_decode($xml->find("description", 0)->innertext)), 0, intval($textlen)) . "&hellip;");
		}
		
		$output .= "</p></div>";
		$output .= "<div class='address'><span>" . $xml->find("address", 0)->innertext . " " . $xml->find("address1", 0)->innertext . ", " . $xml->find("citytown", 0)->innertext  . ", Virginia " . $xml->find("zip", 0)->innertext  . "</span><br /><strong>" . $xml->find("locality", 0)->innertext . "</strong></div>";
		$output .= "</li>";
		
		$z *= -1;
		
		$xml->clear();
	}

	$output = "$output</ul>";


	
	return $output;
	
}

function varetreat_xml_todo($url, $num, $textlen){

	$z = 1; 

	$output .= "<form action=\"$_SERVER[REQUEST_URI]\" method=\"post\" class='search-listings-form'><h2>Filter By:</h2>" . create_dropdown("todo_city") . create_dropdown("todo_locality") . "</form>";

	if($_SERVER["REQUEST_METHOD"] == "POST" ){
		$output .= "<div class='searched-for'>You searched for: " . ucwords($_POST["city"] . $_POST["locality"]) . "</div>";
	}
	
	$output .= "<div class=\"paginations\"></div><ul class=\"zebra\">";
	

	global $wpdb;

	if($_SERVER["REQUEST_METHOD"] == "POST" ){
		if( $_POST["city"] !="" ){ $query_city = " and xml LIKE '%<citytown>" . mysql_real_escape_string($_POST["city"]) . "</citytown>%'"; }
		if( $_POST["locality"] !="" ){ $query_locality = " and xml LIKE '%<locality>" . mysql_real_escape_string($_POST["locality"]) . "</locality>%'"; }
		$response = $wpdb->get_results( "SELECT * FROM varetreat_listings WHERE ( maincategory = 'locAttrVARTRT' or maincategory = 'locDiningVARTRT' or maincategory = 'locShoppingVARTRT' or maincategory = 'locEventsVARTRT' ) $query_city $query_locality ORDER BY title ASC"  );
	} else {
		$response = $wpdb->get_results( "SELECT * FROM varetreat_listings WHERE ( maincategory = 'locAttrVARTRT' or maincategory = 'locDiningVARTRT' or maincategory = 'locShoppingVARTRT' or maincategory = 'locEventsVARTRT' ) ORDER BY title ASC" );
	}
	
	
	foreach ($response as $item){
		
		$xml = str_get_html($item->xml);

		//print "SELECT lat FROM varetreat_listings_latlon_cache WHERE aid = 'a" . $xml->find("attrid", 0)->innertext . "' LIMIT 1";

		$lat = $wpdb->get_var( "SELECT lat FROM varetreat_listings_latlon_cache WHERE aid = 'a" . $xml->find("attrid", 0)->innertext . "' ORDER BY id DESC LIMIT 1" );
		$lon = $wpdb->get_var( "SELECT lon FROM varetreat_listings_latlon_cache WHERE aid = 'a" . $xml->find("attrid", 0)->innertext . "' ORDER BY id DESC LIMIT 1" );
		
		if($z==1){ $output .= "<li class=\"odd\" rel=\"$lat,$lon\" id=\"a" . $xml->find("attrid", 0)->innertext . "\">"; } else { $output .= "<li rel=\"$lat,$lon\" id=\"a" . $xml->find("attrid", 0)->innertext . "\">"; }

		$output .= "<div class='marker'></div><div class='item-content'>";
		
		if( $item->maincategory == "locEventsVARTRT" ){
			$output .= "<b>" . date("m/d/Y", strtotime($item->start)) . " - " . date("m/d/Y", strtotime($item->end)) . "</b>";
		}
		
		$output .=  "	<h3><a href='/listing/" . $xml->find("attrid", 0)->innertext . "/'>" . str_replace("&apos;", "&#39;",$item->title) . "</a></h3>
						<b>" . $xml->find("locality", 0)->innertext . "</b>";
		$output .= "<p>";

		if($textlen == "" || $textlen == "-1"){
			$output .= str_replace("&apos;", "&#39;",strip_tags(html_entity_decode($xml->find("description", 0)->innertext)));
		} else {
			$output .= str_replace("&apos;", "&#39;",substr(strip_tags(html_entity_decode($xml->find("description", 0)->innertext)), 0, intval($textlen)) . "&hellip;");
		}
		
		$output .= "</p></div>";
		$output .= "<div class='address'><span>" . $xml->find("address", 0)->innertext . " " . $xml->find("address1", 0)->innertext . ", " . $xml->find("citytown", 0)->innertext  . ", Virginia " . $xml->find("zip", 0)->innertext  . "</span><br /><strong>" . $xml->find("locality", 0)->innertext . "</strong></div>";
		$output .= "</li>";

		$z *= -1;
		$xml->clear();
		
	}
	
	$output = "$output</ul>";
	
	return $output;
	
}

function varetreat_xml_lodging($url, $num, $textlen){

	$z = 1; 

	$output .= "<form action=\"$_SERVER[REQUEST_URI]\" method=\"post\" class='search-listings-form'><h2>Filter By:</h2>" . create_dropdown("lodging_city") . create_dropdown("lodging_locality") . "</form>";

	if($_SERVER["REQUEST_METHOD"] == "POST" ){
		$output .= "<div class='searched-for'>You searched for: " . ucwords($_POST["city"] . $_POST["locality"]) . "</div>";
	}
	
	$output .= "<div class=\"paginations\"></div><ul class=\"zebra\">";
	

	global $wpdb;

	if($_GET["cid"]!=""){
		$cid = " and xml LIKE '%<lodgingcat>%" . mysql_real_escape_string($_GET["cid"]) . "%</lodgingcat>%'";
	}

	if($_SERVER["REQUEST_METHOD"] == "POST" ){
		if( $_POST["city"] !="" ){ $query_city = " and xml LIKE '%<citytown>" . mysql_real_escape_string($_POST["city"]) . "</citytown>%'"; }
		if( $_POST["locality"] !="" ){ $query_locality = " and xml LIKE '%<locality>" . mysql_real_escape_string($_POST["locality"]) . "</locality>%'"; }
		$response = $wpdb->get_results( "SELECT * FROM varetreat_listings WHERE ( maincategory = 'locLodgingVARTRT' ) $query_city $query_locality $cid ORDER BY title ASC"  );
	} else {
		$response = $wpdb->get_results( "SELECT * FROM varetreat_listings WHERE ( maincategory = 'locLodgingVARTRT' ) $cid ORDER BY title ASC" );
	}
	
	
	foreach ($response as $item){
		
		$xml = str_get_html($item->xml);

		$lat = $wpdb->get_var( "SELECT lat FROM varetreat_listings_latlon_cache WHERE aid = 'a" . $xml->find("attrid", 0)->innertext . "' ORDER BY id DESC LIMIT 1" );
		$lon = $wpdb->get_var( "SELECT lon FROM varetreat_listings_latlon_cache WHERE aid = 'a" . $xml->find("attrid", 0)->innertext . "' ORDER BY id DESC LIMIT 1" );
		
		if($z==1){ $output .= "<li class=\"odd\" rel=\"$lat,$lon\" id=\"a" . $xml->find("attrid", 0)->innertext . "\">"; } else { $output .= "<li rel=\"$lat,$lon\" id=\"a" . $xml->find("attrid", 0)->innertext . "\">"; }

		$output .= "<div class='marker'></div><div class='item-content'>";
		
		$output .=  "	<h3><a href='/listing/" . $xml->find("attrid", 0)->innertext . "/'>" . str_replace("&apos;", "&#39;",$item->title) . "</a></h3>
						<b>" . $xml->find("locality", 0)->innertext . "</b>";
		$output .= "<p>";

		if($textlen == "" || $textlen == "-1"){
			$output .= str_replace("&apos;", "&#39;",strip_tags(html_entity_decode($xml->find("description", 0)->innertext)));
		} else {
			$output .= str_replace("&apos;", "&#39;",substr(strip_tags(html_entity_decode($xml->find("description", 0)->innertext)), 0, intval($textlen)) . "&hellip;");
		}
		
		$output .= "</p></div>";
		$output .= "<div class='address'><span>" . $xml->find("address", 0)->innertext . " " . $xml->find("address1", 0)->innertext . ", " . $xml->find("citytown", 0)->innertext  . ", Virginia " . $xml->find("zip", 0)->innertext  . "</span><br /><strong>" . $xml->find("locality", 0)->innertext . "</strong></div>";
		$output .= "</li>";

		$z *= -1;
		$xml->clear();
		
	}
	
	$output = "$output</ul>";
	
	return $output;
	
}

function varetreat_xml_search($url, $num, $textlen){
	global $wpdb;

	if( $_GET["s"] !="" ){ $query_city = " xml LIKE '%" . mysql_real_escape_string($_GET["s"]) . "%'"; }
	$response = $wpdb->get_results( "SELECT * FROM varetreat_listings WHERE $query_city ORDER BY title ASC"  );
	
	foreach ($response as $item){
		$xml = str_get_html($item->xml);

		$output .= "<div class=\"page type-page status-publish hentry\"><h3 class=\"entry-title\"><a href=\"" . get_bloginfo('url') . "/listing/" . $xml->find("attrid",0)->innertext . "\">" . $xml->find("title",0)->innertext . "</a></h3><div class=\"entry-summary\"><p>";
			if($textlen == "" || $textlen == "-1"){
				$output .= strip_tags(html_entity_decode($xml->find("description", 0)->innertext));
			} else {
				$output .= substr(strip_tags(html_entity_decode($xml->find("description", 0)->innertext)), 0, intval($textlen)) . " [...]";
			}
		$output .= "</p><p class=\"url\"><a href=\"" . get_bloginfo('url') . "/listing/" . $xml->find("attrid",0)->innertext . "/\">" . get_bloginfo('url') . "/listing/" . $xml->find("attrid",0)->innertext . "/</a></p></div></div>";
		
		$xml->clear();
	}
	return $output;
}

function varetreat_xml_single_feed($atts, $content = null) {
	extract(shortcode_atts(array(
		"url" => ""
	), $atts));

	return varetreat_xml($url, -1, 200);
}
add_shortcode("listings", "varetreat_xml_single_feed");

// split content at the more tag and return an array
function split_content() {
	global $more;
	$more = true;
	$content = preg_split('/<span id="more-\d+"><\/span>/i', get_the_content('more'));
	for($c = 0, $csize = count($content); $c < $csize; $c++) {
		$content[$c] = apply_filters('the_content', $content[$c]);
	}
	return $content;
}


if ( function_exists('register_sidebar') )
register_sidebar(array(
'before_widget' => '<div id="%1$s" class="widget %2$s">',
'after_widget' => '</div>',
));



ob_start();

?>
