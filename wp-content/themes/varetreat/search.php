<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

	$z = 0;

get_header(); ?>

<style type="text/css" media="screen">
	
	#combined-search .pagination{
		text-align: center;
	}
	
	.no-results{
		display: none;
	}
	
</style>

<script type="text/javascript" charset="utf-8">

	$(document).ready(function() {
		if(  $("#combined-search .hentry").length == 0  ){
			$(".no-results").css("display", "block");
		}
		
		if(  $("#combined-search .hentry").length < 11  ){
			$(".paginations").css("display", "none");
		}
		
	});
	
	
	
</script>

<div id="container">
	<div class="container_12 clearfix">
		<div class="grid_9">
			<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
			

			<h2 class="page-title"><?php printf( __( 'Search Results for: %s', 'twentyten' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
			<form action="<?php $bloginfo = get_bloginfo('url'); ?>" method="get">
				<fieldset>
					<input class="bigsearch" type="text" name="s" id="search" value="search again" />
				</fieldset>
			</form>
			<hr/>
			
			<div id="combined-search">
			
			
			
			<?php
			global $query_string;
			query_posts( $query_string . '&posts_per_page=-1');
			?>
			<?php if ( have_posts() ) : ?>
				<?php
				/* Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called loop-search.php and that will be used instead.
				 */
				 get_template_part( 'loop', 'search' );
				$z++;
				?>
			<?php endif; ?>
			
			<?php print varetreat_xml_search("", "", "200"); ?>

			<div class="paginations"></div>
			
			</div>
			
			<div name="paginationoptions" style="display: none;">
				<input type="hidden" value="10" name="items_per_page" id="items_per_page" class="numeric"/>
				<input type="hidden" value="10" name="num_display_entries" id="num_display_entries" class="numeric"/>
				<input type="hidden" value="2" name="num_edge_entries" id="num_edge_entries" class="numeric"/>
				<input type="hidden" value="Prev" name="prev_text" id="prev_text"/>
				<input type="hidden" value="Next" name="next_text" id="next_text"/>
			</div>
			

				
				  <div id="post-0" class="post no-results not-found">
					<h2 class="entry-title"><?php _e( 'Nothing Found', 'twentyten' ); ?></h2>
					<div class="entry-content">
						<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'twentyten' ); ?></p>
					</div><!-- .entry-content -->
				</div><!-- #post-0 -->
				

			
		</div><!-- #content -->
	</div><!-- #container -->
</div>
<?php get_footer(); ?>
