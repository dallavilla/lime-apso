<div id="container" class="full-detail">
	<div class="container_12 clearfix">
		<div class="grid_12">
			<h2><?php print $element->getElementsByTagName('title')->item(0)->nodeValue ?></h2>
			<?php if($item->maincategory == "locEventsVARTRT"){
				print "<h3>" . $item->start . " - " . $item->end . "</h3>";
			} ?>
			
			<p><strong>Address:</strong><br />
			<?php print $element->getElementsByTagName('address')->item(0)->nodeValue ?><?php print $element->getElementsByTagName('address1')->item(0)->nodeValue ?><br />
			<?php print $element->getElementsByTagName('citytown')->item(0)->nodeValue ?>, Virginia <?php print $element->getElementsByTagName('zip')->item(0)->nodeValue ?></p>
		</div>
		<div class="grid_9">
			<p><?php print html_entity_decode($element->getElementsByTagName('description')->item(0)->nodeValue); ?></p>
			<?php if ($element->getElementsByTagName('email')->item(0)->nodeValue != '') { ?>
				<p><strong>Email:</strong><br />
				<a href="mailto:<?php print $element->getElementsByTagName('email')->item(0)->nodeValue ?>"><?php print $element->getElementsByTagName('email')->item(0)->nodeValue ?></a></p>
			<?php } ?>
			<?php if ($element->getElementsByTagName('url')->item(0)->nodeValue != '') { ?>
				<p><strong>Website:</strong><br />
				<a href="http://<?php print $element->getElementsByTagName('url')->item(0)->nodeValue ?>" target="_blank"><?php print $element->getElementsByTagName('url')->item(0)->nodeValue ?></a>
				<!--<a href="<?php bloginfo('url'); ?>/listing/<?php print $element->getElementsByTagName('attrid')->item(0)->nodeValue ?>" target="_blank"><?php bloginfo('url'); ?>/listing/<?php print $element->getElementsByTagName('attrid')->item(0)->nodeValue ?></a>-->
				</p>
			<?php } ?>
		</div>
		<div class="grid_3">
			<div class="share"><?php addthis(); ?></div>
			
			<?php if ($element->getElementsByTagName('imgname')->item(0)->nodeValue != '') { ?>
				<p><img src="<?php print $element->getElementsByTagName('imgname')->item(0)->nodeValue ?>" class="shadow" alt="<?php print $element->getElementsByTagName('title')->item(0)->nodeValue ?>"/></p>
			<?php } ?>
			
			<p><strong>Phone:</strong><br />
			<?php print "(" . substr($element->getElementsByTagName('phone1')->item(0)->nodeValue, 0, 3) . ") " . substr($element->getElementsByTagName('phone1')->item(0)->nodeValue, 3, 3) . "-" . substr($element->getElementsByTagName('phone1')->item(0)->nodeValue, 6); ?></p>
			
			<?php if ($element->getElementsByTagName('fax')->item(0)->nodeValue != '') { ?>
				<p><strong>Fax:</strong><br />
				<?php print "(" . substr($element->getElementsByTagName('fax')->item(0)->nodeValue, 0, 3) . ") " . substr($element->getElementsByTagName('fax')->item(0)->nodeValue, 3, 3) . "-" . substr($element->getElementsByTagName('fax')->item(0)->nodeValue, 6); ?></p>
			<?php } ?>
			
			<?php if ($element->getElementsByTagName('hropen')->item(0)->nodeValue != '') { ?>
				<p><strong>Hours of Operation:</strong><br />
				<?php print $element->getElementsByTagName('hropen')->item(0)->nodeValue ?> - <?php print $element->getElementsByTagName('hrclose')->item(0)->nodeValue ?></p>
			<?php } ?>


		   
			<!--
			<p><strong>Price</strong>:<br />
			<?php print $element->getElementsByTagName('price')->item(0)->nodeValue ?><br />
			</p>
			-->

			<ul class="moreinfo">
			
				<?php if ($element->getElementsByTagName('famfriendly')->item(0)->nodeValue == 'True') { ?>
					<li>Bring the whole family!</li>
				<?php } ?>

				<?php if ($element->getElementsByTagName('petfriendly')->item(0)->nodeValue == 'True') { ?>
					<li>Bring your pets!</li>
				<?php } ?>

				<?php if ($element->getElementsByTagName('apets')->item(0)->nodeValue == 'True') { ?>
					<li>Bring your pets!</li>
				<?php } ?>

				<?php if ($element->getElementsByTagName('acontbreakfast')->item(0)->nodeValue == 'True') { ?>
					<li>Continental Breakfast</li>
				<?php } ?>

				<?php if ($element->getElementsByTagName('aexerciseroom')->item(0)->nodeValue == 'True') { ?>
					<li>Exercise Room</li>
				<?php } ?>

				<?php if ($element->getElementsByTagName('ajacuzzi')->item(0)->nodeValue == 'True') { ?>
					<li>Jacuzzi</li>
				<?php } ?>

				<?php if ($element->getElementsByTagName('aindoorpool')->item(0)->nodeValue == 'True') { ?>
					<li>Indoor Pool</li>
				<?php } ?>

				<?php if ($element->getElementsByTagName('aoutdoorpool')->item(0)->nodeValue == 'True') { ?>
					<li>Outdoor Pool</li>
				<?php } ?>
			
			</ul>
			
			<?php
			
				if ($element->getElementsByTagName('wheelchair')->item(0)->nodeValue == 'True') { $access .= "<li>Wheelchair Accessible</li>"; } 

				if ($element->getElementsByTagName('lowvision')->item(0)->nodeValue == 'True') { $access .= "<li>Low Vision Accessible</li>"; } 

				if ($element->getElementsByTagName('signlang')->item(0)->nodeValue == 'True') { $access .= "<li>Sign Language Accessible</li>"; } 

				if ($element->getElementsByTagName('teletype')->item(0)->nodeValue == 'True') { $access .= "<li>TeleType Accessible</li>"; } 

				if ($element->getElementsByTagName('assistivelisten')->item(0)->nodeValue == 'True') { $access .= "<li>Assistive Listen Accessible</li>"; } 

				if ($element->getElementsByTagName('audiodevice')->item(0)->nodeValue == 'True') { $access .= "<li>Audio Device Accessible</li>"; } 

				if ($element->getElementsByTagName('braille')->item(0)->nodeValue == 'True') { $access .= "<li>Braille Accessible</li>"; }
			    
				if($access!=""){
					print "<strong>Accessibility</strong>:<br /><ul class=\"moreinfo\">" . $access ."</ul>";
				}
			
			?>

		</div>
	</div>
</div>

<!--

	ERIK:
	**************************************************************
	Use the syntax for retrieving the data as used above... you
	can use standard php functions on these strings if you need
	to crop, compare, etc.  A full list of the variables for
	a given listing are found below.  Style to your heart's
	content. -Jay 3/9/2011
	
	title
	link
	attrID
	address1
	address2
	citytown
	locality
	zip
	phone1
	phone1desc
	phone2
	phone2desc
	phone3
	phone3desc
	fax
	email
	url
	BookingURL
	imgName
	imgCaption
	daterange
	startdate
	enddate
	hrOpen
	hrClose
	description
	attrCat
	Price
	PetFriendly
	FamFriendly
	WheelChair
	LowVision
	SignLang
	TeleType
	AssistiveListen
	AudioDevice
	Braille
	LastModified

	**************************************************************

-->