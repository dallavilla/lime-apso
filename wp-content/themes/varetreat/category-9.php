<?php get_header(); ?>
<div id="container">

<div class="search-results">
	
	<table>
		<tr>
			<td>
				<img src="<?php bloginfo('template_directory'); ?>/images/highlight-lodging.jpg">
			</td>
			<td>
				<h2 style="margin: 40px 0px 16px 0px">Where to Stay</h2>
				<div style="padding-right: 150px; font-size: 14px;"><?php echo category_description(9); ?></div>
			</td>
		</tr>
		<tr>
			<td class="search-categories">
				
				<div>
					<h2>Categories</h2>
				
					<h3><a href="/category/where-to-stay/"  <?php if($_GET["cid"]==""){ print " class='selected'"; } ?>>See All</a></h3>
					<?php
						/*
							$args=array(
								'post_type' => 'post',
								'post_status' => 'publish',
								'posts_per_page' => 20,
								'caller_get_posts'=> 1,
								'category_name' => 'What To Do',
								'orderby' => 'title',
								'order' => 'asc'
							);
					
						    $my_query = null;
						    $my_query = new WP_Query($args);
						    if( $my_query->have_posts() ) {
								while ($my_query->have_posts()) : $my_query->the_post(); ?>
									<h3 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
								<?php
								endwhile;
							}
							wp_reset_query();
						*/
						
						include($_SERVER["DOCUMENT_ROOT"] . '/wp-content/themes/varetreat/var_categories.php');
						$cats = explode(",", $lodging_type);
						
						foreach($cats as $cat){
							?>
							<h3><a href="/category/where-to-stay/?cid=<?php print urlencode($cat); ?>" <?php if($cat == $_GET["cid"]){ print " class='selected'"; } ?>><?php print ucwords($cat); ?></a></h3>
							<?php
						}
					?>
				</div>
			</td>
			<td>
				
				<div class="search-listings">
					
					<div id="google_map"></div>
					
					<div id="search_results">

						<?php print varetreat_xml_lodging("", "", "200"); ?>

						<div class="paginations"></div>

					</div>

					

					<div name="paginationoptions" style="display: none;">
						<input type="hidden" value="20" name="items_per_page" id="items_per_page" class="numeric"/>
						<input type="hidden" value="10" name="num_display_entries" id="num_display_entries" class="numeric"/>
						<input type="hidden" value="2" name="num_edge_entries" id="num_edge_entries" class="numeric"/>
						<input type="hidden" value="Prev" name="prev_text" id="prev_text"/>
						<input type="hidden" value="Next" name="next_text" id="next_text"/>
					</div>
					
					
					
					
				</div>
				
			</td>
		</tr>
	</table>
	
	
	<script type="text/javascript" charset="utf-8">

		$(document).ready(function() {
			if( $(".zebra li").length < 21 ) { $(".paginations").css("display", "none"); }
		});
		
	</script>
	
	
</div>
	
</div>
<?php get_footer(); ?>