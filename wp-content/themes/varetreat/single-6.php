<?php get_header(); ?>
<div id="container" class="adventure">
	<div class="container_12 clearfix">
	<?php if ( get_post_meta($post->ID, 'hero', true) ) : ?>
		<img class="hero" src="<?php echo get_post_meta($post->ID, 'hero', true) ?>" alt="<?php the_title_attribute(); ?>" />
	<?php endif; ?>
		<div class="grid_12">
			
			<div class="share"><?php addthis(); ?></div>
			
			<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
		</div>
		<div class="grid_6">
			<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>
				<div class="section">
					<h2><?php the_title(); ?></h2>
					<?php split_content(); ?>
					<?php
					// original content display
					// the_content('<p>Read the rest of this page &raquo;</p>');
					// split content into array
					$content = split_content();
					// output first content section in column1
					echo '<div id="column1">', array_shift($content), '</div>';
					// output remaining content sections in column2
					?>
				</div>
				
			<?php endwhile; ?>
			<?php else : ?>
				<h2>Sorry, the entry you are looking for is not here. <a href="<?php echo get_option('url'); ?>">Return Home</a>.</h2>
			<?php endif; ?>
		</div>
		<div class="grid_6">
			<?php echo '<div id="column2">', implode($content), '</div>'; ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>