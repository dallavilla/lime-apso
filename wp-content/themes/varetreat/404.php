<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

	<div id="container">
		<div class="container_12" style="height: 400px;">

				<h1 class="entry-title"><?php _e( 'Not Found', 'twentyten' ); ?></h1>
					<p><?php _e( 'Apologies, but the page you requested could not be found. Perhaps searching will help.', 'twentyten' ); ?></p>

		</div><!-- #content -->
	</div><!-- #container -->

<?php get_footer(); ?>