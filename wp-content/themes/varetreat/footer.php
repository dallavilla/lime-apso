<div class="clear"></div>
</div>
<div id="footer">
	<div class="container_12">
		<div class="grid_12">
			<p class="auxnav"><a href="<?php bloginfo('url'); ?>">Home</a> | <a href="<?php echo get_category_link(3); ?>">About the Region</a> | <a href="<?php echo get_category_link(8); ?>">What to Do</a> | <a href="<?php echo get_category_link(9); ?>">Where to Stay</a> | <a href="<?php echo get_category_link(6); ?>">History Adventures</a> | <a href="<?php echo get_category_link(7); ?>">Outdoor Adventures</a> | <a href="<?php echo get_page_link(10); ?>">Contact Us</a></p>
			<span style="float:left; padding-bottom:18px; padding-right: 22px;"><?php get_search_form(); ?></span>
			<p class="clearleft">Copyright &copy; <?php echo date('Y'); ?> <?php bloginfo('name');?></p>
		</div>
	</div>
</div>
</body>
</html>