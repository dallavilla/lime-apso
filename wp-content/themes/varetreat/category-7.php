<?php get_header(); ?>
<div id="container">
	<div class="container_12 clearfix">
		<div id="slideshow">
			<ul id="nav">
				<li id="prev"><a href="#">Previous</a></li>
				<li id="next"><a href="#">Next</a></li>
			</ul>
			<ul id="slides">
				<li><img src="<?php bloginfo('template_directory'); ?>/images/feeltherush.jpg" alt="Feel the Rush" /></li>
				<li><a href="/adventures/outdoor-adventures/paddle-hike-and-celebrate-2/"><img src="<?php bloginfo('url'); ?>/wp-content/uploads/2011/03/paddle.jpg" alt="Paddle, Hike and Celebrate" /></a></li>
				<li><a href="/adventures/outdoor-adventures/picnic-adventure/"><img src="<?php bloginfo('url'); ?>/wp-content/uploads/2011/03/picnic.jpg" alt="Picnic Adventure" /></a></li>
				<li><a href="/adventures/outdoor-adventures/camp-re-enacted-2/"><img src="<?php bloginfo('url'); ?>/wp-content/uploads/2011/03/campreenacted.jpg" alt="Camp Re-Enacted" /></a></li>
				<li><a href="/adventures/outdoor-adventures/farm-life-reverie-2/"><img src="<?php bloginfo('url'); ?>/wp-content/uploads/2011/03/farmlife.jpg" alt="Farm Life Reverie" /></a></li>
			</ul>
		</div>
		<div class="grid_9">
			<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
			<h2>Let Nature Bring Out The Adventurer In You</h2>
			<p><?php echo category_description(7); ?></p>
		</div>
		<hr />
		<?php
		 $posts = query_posts($query_string . 
		'&orderby=title&order=asc&posts_per_page=-1'); 
		// here comes The Loop!
		if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div class="grid_6 postbox" style="margin-bottom: 20px">
			<?php if ( has_post_thumbnail()) : ?>
				<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
				<?php the_post_thumbnail('large', array('class' => 'shadow floatleft')); ?>
				</a>
			<?php endif; ?>
			<h3 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
			<?php the_excerpt(''); ?>
			<p class="url"><a class="more" href="<?php the_permalink(); ?>">click for more info</a></p>
		</div>
		<?php endwhile; ?>
		<?php else : ?>
		<div class="grid_12"><h2>Sorry, there are no news entries at this time.</h2></div>
		<?php endif; ?>
		<hr />
		<div class="grid_6">
			<h3>Outdoor Attractions</h3>
			<p>Virginia's Retreat boasts 10 Virginia State Parks and two National Parks, along with numerous municipal parks and access areas. The James and Appomattox Rivers wind through the region, as well as many streams and tributaries perfect for kayaking or canoeing. In fact, you're never far from water here, with lakes of all sizes – including watersports meccas <a href="http://www.virginia.org/site/description.asp?attrid=20291">Buggs Island Lake</a> and <a href="/listing/15264/?f=locAttrVARTRT">Lake Gaston</a>. Even motorsports here deliver natural inspiration, such as <a href="/listing/19951/?f=locAttrVARTRT">Virginia International Raceway's</a> spectacular setting.</p>
			<p>Trails throughout the region allow you to explore with confidence, bringing you up close and personal with wildlife, unspoiled nature, and sometimes the remnants of an earlier time. History and outdoors intersect in many places in Virginia's Retreat. Wildlife Management Areas offer acres and acres of field and forest habitats, some with riding trails and fishing areas.</p>
			<p><a class="more" href="<?php echo get_category_link(8); ?>">click for more info on what to do</a></p>
		</div>
		<div class="grid_6">
			<h3>Upcoming Events</h3>
			<div id="eventspromo">
				<?php print varetreat_events('http://www.virginia.org/events/xml/VARetreat/locEventsVARTRT.xml', 5, 200 ); ?>
				<p><a class="more" href="<?php echo get_category_link(8); ?>">click for all event listings</a></p>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>