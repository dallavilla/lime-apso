<?php get_header(); ?>
<div id="container">
	<div class="container_12 clearfix">
		<div class="grid_9">
		<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
	<?php if (have_posts()) : ?>
			<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
			<?php /* If this is a category archive */ if (is_category()) { ?>
			<div class="section"><h2><?php echo single_cat_title(); ?></h2></div>

			<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
			<div class="section"><h2>Archive for <?php the_time('F jS, Y'); ?>&#58;</h2></div>

			<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
			<div class="section"><h2>Archive for <?php the_time('F, Y'); ?>&#58;</h2></div>

			<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
			<div class="section"><h2>Archive for <?php the_time('Y'); ?>&#58;</h2></div>

			<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>

			<?php } ?>
			<?php while (have_posts()) : the_post(); ?>
			<div class="section">
				<p class="date"><?php the_time('F jS, Y'); ?></p>
				<h2 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				<?php the_content('+ Read More'); ?>
			</div>
		<?php endwhile; ?>
	<?php else : ?>
		<h2>Sorry, there are no entries that meet that criteria.</h2>
	<?php endif; ?>
		</div>
    </div>
</div>
<?php get_footer(); ?>