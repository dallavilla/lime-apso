// JavaScript Document
$(document).ready(function() {
	
	if($("#google_map").length>0){
			gmsr_init();
	}
	
	
   $("input[type=text]").focus(function() {
       if (this.value == this.defaultValue) {
           this.value = "";
       }
   }).blur(function() {
       if (!this.value.length) {
           this.value = this.defaultValue;
       }
   });


	if($("#search_results").length>0){
		var optInit = getOptionsFromForm();
		$(".paginations").pagination($(".zebra li").length, optInit);
	}
	
	if($("#combined-search").length>0){
		var optInit = getOptionsFromSearchForm();
		$(".paginations").pagination($("#combined-search .hentry").length, optInit);
	}
	
	
	


	$("a").each(function(){
		if( $(this).attr("href").indexOf(".pdf") > 1 ){
			$(this).attr("target", "_blank");
		}
	});

});


var google_map;
var infowindow; //  = new google.maps.InfoWindow({ content: "" });
var geocoder;
var markersArray = [];

var lat_min = 0.0;
var lat_max = 0.0;
var lng_min = 0.0;
var lng_max = 0.0;


function gup(name){
	name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	var regexS = "[\\?&]"+name+"=([^&#]*)";
	var regex = new RegExp( regexS );
	var results = regex.exec( window.location.href );
	if( results == null )
		return "";
	else
		return results[1];
}


function getOptionsFromForm(){
    var opt = {callback: pageselectCallback};
    $("input:hidden").each(function(){ opt[this.name] = this.className.match(/numeric/) ? parseInt(this.value) : this.value; });
    var htmlspecialchars ={ "&":"&amp;", "<":"&lt;", ">":"&gt;", '"':"&quot;"}
    $.each(htmlspecialchars, function(k,v){
        opt.prev_text = opt.prev_text.replace(k,v);
        opt.next_text = opt.next_text.replace(k,v);
    })
    return opt;
}

function pageselectCallback(page_index, jq){
    var items_per_page = $('#items_per_page').val();
    var max_elem = Math.min((page_index+1) * items_per_page, $(".zebra li").length);
	$(".zebra li").css("display", "none");
    for(var i=page_index*items_per_page;i<max_elem;i++){ $(".zebra li:eq(" + i + ")").css("display", "block"); }

	googlemarkers();

	if(page_index>0){
		scrollTo(0,300);
	}

    return false;
} 

function pageselectCallbackSearch(page_index, jq){
    var items_per_page = $('#items_per_page').val();
    var max_elem = Math.min((page_index+1) * items_per_page, $("#combined-search .hentry").length);
	$("#combined-search .hentry").css("display", "none");
    for(var i=page_index*items_per_page;i<max_elem;i++){ $("#combined-search .hentry:eq(" + i + ")").css("display", "block"); }

	if(page_index>0){
		scrollTo(0,0);
	}

    return false;
}

function getOptionsFromForm(){
    var opt = {callback: pageselectCallback};
    $("input:hidden").each(function(){ opt[this.name] = this.className.match(/numeric/) ? parseInt(this.value) : this.value; });
    var htmlspecialchars ={ "&":"&amp;", "<":"&lt;", ">":"&gt;", '"':"&quot;"}
    $.each(htmlspecialchars, function(k,v){
        opt.prev_text = opt.prev_text.replace(k,v);
        opt.next_text = opt.next_text.replace(k,v);
    })
    return opt;
}

function getOptionsFromSearchForm(){
    var opt = {callback: pageselectCallbackSearch};
    $("input:hidden").each(function(){ opt[this.name] = this.className.match(/numeric/) ? parseInt(this.value) : this.value; });
    var htmlspecialchars ={ "&":"&amp;", "<":"&lt;", ">":"&gt;", '"':"&quot;"}
    $.each(htmlspecialchars, function(k,v){
        opt.prev_text = opt.prev_text.replace(k,v);
        opt.next_text = opt.next_text.replace(k,v);
    })
    return opt;
}


function clickMarker(id){
	google.maps.event.trigger(window['marker' + id], 'click');
	scrollTo(0, 300);
	return false;
}

function deleteOverlays() {
  if (markersArray) {
    for (i in markersArray) {
      window[markersArray[i]].setMap(null);
    }
    markersArray.length = 0;
  }
}

function autoAdjustMap(){

	google_map.setCenter(new google.maps.LatLng( ((lat_max + lat_min) / 2.0), ((lng_max + lng_min) / 2.0) ));
	google_map.fitBounds(new google.maps.LatLngBounds( new google.maps.LatLng(lat_min, lng_min), new google.maps.LatLng(lat_max, lng_max) ));

	zoomChangeBoundsListener = google.maps.event.addListener(google_map, 'bounds_changed', function(event) {
	        if (google_map.getZoom() > 12){
	            google_map.setZoom(12); 
	        }
	    google.maps.event.removeListener(zoomChangeBoundsListener);
	});
	
}


function googlemarkers(){

	deleteOverlays();
    
	lat_min = 0.0;
	lat_max = 0.0;
	lng_min = 0.0;
	lng_max = 0.0;
	
	letterid = 65;
	$(".zebra li").each(function(){

		if($(this).attr("style").toString().indexOf("block")>1){

			aid = $(this).attr("id");
			address = $(this).find(".address").html();
			addressProper = $(this).find(".address span").html();
			latlon = $(this).attr("rel").toString().split(",");
			mtitle = $(this).find("h3").text();
			mlink = $(this).find("h3").html();
		
			if($(this).attr("rel")!=","){
				
				$(this).find(".marker").html(" <a href=\"#\" onclick=\"return clickMarker('" + aid + "');\"><img src='http://www.google.com/mapfiles/marker" + String.fromCharCode(letterid) + ".png' /></a> ");

				// alert(latlon[0] + " -- " + latlon[1] + " -- " + mtitle);
				
				if(parseFloat(latlon[0]) > lat_max){ lat_max = latlon[0]; }
				if(parseFloat(latlon[0]) < lat_min || lat_min == 0){ lat_min = latlon[0]; }
				
				if(parseFloat(latlon[1]) > lng_max || lng_min == 0){ lng_max = latlon[1]; }
				if(parseFloat(latlon[1]) < lng_min || lng_min == 0){ lng_min = latlon[1]; }
				

				window["marker" + aid] = new google.maps.Marker({
					position: new google.maps.LatLng(latlon[0], latlon[1]), 
					map: google_map,
					icon: "http://www.google.com/mapfiles/marker" + String.fromCharCode(letterid) + ".png", 
					title: mtitle,
					vartdesc: "<p><a href=\"/listing/" + aid.replace("a", "") + "/\">" + mtitle + "</a><br/>" + address + "</p>"
				});
				
				markersArray.push("marker" + aid);
				
				google.maps.event.addListener(window["marker" + aid], 'click', function() {
					if (infowindow) infowindow.close();
					infowindow.setOptions({ maxWidth: 200 });
					infowindow.setContent( this.vartdesc );
					infowindow.open(google_map,this);
				});
				
			} else {
				
				meh = $.ajax({
						url: "/wp-content/themes/varetreat/cache-latlon.php?id=" + aid + "&address=" + addressProper.replace("&amp;", "and"),
						async: false
					}).responseText;
					
				// alert ("This is meh:" + meh);
					
			}
			
			letterid++;
			
		}
		
		
		// if($(this).attr("style")=="display: block;"){
		// 	
		// 	letterid++;
		// 	
		// 	if($(this).attr("rel")!=","){
		// 	
		// 	geocoder = new google.maps.Geocoder();
		// 	
		// 	var address = $(this).find(".address").html();
		// 	alert(address + " -- " + aid);
        // 
		// 	
		// 	
		// 	
        // 
		// 	
		// 	}
		// 	
		// 	
		// }
		
		
	});

	autoAdjustMap();
	google.maps.event.trigger(google_map, "resize"); 

}




function gmsr_init(){
	var lat = 37.342;
	var lon = -78.58; 
	var mapzoom = 7;

	if( $("#google_map").attr("rel")!=undefined && $("#google_map").attr("rel").indexOf(",")!=-1 ){
		var latlon_temp = $("#google_map").attr("rel").split(",");
		lat = parseFloat(latlon_temp[0]);
		lon = parseFloat(latlon_temp[1]);
		mapzoom = parseInt(latlon_temp[2]);
	}

	var latlng = new google.maps.LatLng(lat, lon);
	var myOptions = {
		zoom: mapzoom,
		center: latlng,
		mapTypeControl: false,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	
	google_map = new google.maps.Map(document.getElementById("google_map"), myOptions);
	infowindow = new google.maps.InfoWindow({ content: "" });
}



function isDate(txtDate) {
    var objDate,  // date object initialized from the txtDate string
        mSeconds, // txtDate in milliseconds
        day,      // day
        month,    // month
        year;     // year
    // date length should be 10 characters (no more no less)
    if (txtDate.length !== 10) {
        return false;
    }
    // third and sixth character should be '/'
    if (txtDate.substring(2, 3) !== '/' || txtDate.substring(5, 6) !== '/') {
        return false;
    }
    // extract month, day and year from the txtDate (expected format is mm/dd/yyyy)
    // subtraction will cast variables to integer implicitly (needed
    // for !== comparing)
    month = txtDate.substring(0, 2) - 1; // because months in JS start from 0
    day = txtDate.substring(3, 5) - 0;
    year = txtDate.substring(6, 10) - 0;
    // test year range
    if (year < 1000 || year > 3000) {
        return false;
    }
    // convert txtDate to milliseconds
    mSeconds = (new Date(year, month, day)).getTime();
    // initialize Date() object from calculated milliseconds
    objDate = new Date();
    objDate.setTime(mSeconds);
    // compare input date and parts from Date() object
    // if difference exists then date isn't valid
    if (objDate.getFullYear() !== year ||
        objDate.getMonth() !== month ||
        objDate.getDate() !== day) {
        return false;
    }
    // otherwise return true
    return true;
}

function checkDates(){
    retval = true;

    if( !isDate( $("#sdate").val() )) {
		alert("Invalid Start Date");
		retval = false;
    }

    if( !isDate( $("#edate").val() )) {
		alert("Invalid End Date");
		retval = false;
    }

	return retval;

}

