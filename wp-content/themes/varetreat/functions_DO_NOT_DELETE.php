<?php 
// Disable WP jQuery
if ( function_exists( 'add_theme_support' ) ) { 
  add_theme_support( 'post-thumbnails' ); 
}

if( !is_admin()){
       wp_deregister_script('jquery');
       wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"), false, '1.5.1');
       wp_enqueue_script('jquery');
}

/* i forget */
add_filter('single_template', create_function('$t', 'foreach( (array) get_the_category() as $cat ) { if ( file_exists(TEMPLATEPATH . "/single-{$cat->term_id}.php") ) return TEMPLATEPATH . "/single-{$cat->term_id}.php"; } return $t;' ));

/* sets length of the excerpt */
function new_excerpt_length($length) {
	return 30;
}
add_filter('excerpt_length', 'new_excerpt_length');

/* breadcrumbs */
function dimox_breadcrumbs() {
 
  $delimiter = '&raquo;';
  $home = 'Home'; // text for the 'Home' link
  $before = '<span class="current">'; // tag before the current crumb
  $after = '</span>'; // tag after the current crumb
 
  if ( !is_home() && !is_front_page() || is_paged() ) {
 
    echo '<div id="crumbs">';
 
    global $post;
    $homeLink = get_bloginfo('url');
    echo '<a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';
 
    if ( is_category() ) {
      global $wp_query;
      $cat_obj = $wp_query->get_queried_object();
      $thisCat = $cat_obj->term_id;
      $thisCat = get_category($thisCat);
      $parentCat = get_category($thisCat->parent);
      if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
      echo $before . single_cat_title('', false) . $after;
 
    } elseif ( is_day() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('d') . $after;
 
    } elseif ( is_month() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('F') . $after;
 
    } elseif ( is_year() ) {
      echo $before . get_the_time('Y') . $after;
 
    } elseif ( is_single() && !is_attachment() ) {
      if ( get_post_type() != 'post' ) {
        $post_type = get_post_type_object(get_post_type());
        $slug = $post_type->rewrite;
        echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a> ' . $delimiter . ' ';
        echo $before . get_the_title() . $after;
      } else {
        $cat = get_the_category(); $cat = $cat[0];
        echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
        echo $before . get_the_title() . $after;
      }
 
    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' ) {
      $post_type = get_post_type_object(get_post_type());
      echo $before . $post_type->labels->singular_name . $after;
 
    } elseif ( is_attachment() ) {
      $parent = get_post($post->post_parent);
      $cat = get_the_category($parent->ID); $cat = $cat[0];
      echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
      echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a> ' . $delimiter . ' ';
      echo $before . get_the_title() . $after;
 
    } elseif ( is_page() && !$post->post_parent ) {
      echo $before . get_the_title() . $after;
 
    } elseif ( is_page() && $post->post_parent ) {
      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
      echo $before . get_the_title() . $after;
 
    } elseif ( is_search() ) {
      echo $before . 'Search results for "' . get_search_query() . '"' . $after;
 
    } elseif ( is_tag() ) {
      echo $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;
 
    } elseif ( is_author() ) {
       global $author;
      $userdata = get_userdata($author);
      echo $before . 'Articles posted by ' . $userdata->display_name . $after;
 
    } elseif ( is_404() ) {
      echo $before . 'Error 404' . $after;
    }
 
    if ( get_query_var('paged') ) {
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
      echo __('Page') . ' ' . get_query_var('paged');
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    }
 
    echo '</div>';
 
  }
} // end dimox_breadcrumbs()

/* XML Parser */

include_once("simple_html_dom.php");


function varetreat_xml_single_page(){
	global $blog_id;
	global $wpdb;

	$currpage = "http://" . $_SERVER["HTTP_HOST"] . $_SERVER['REQUEST_URI'];
	if(!is_admin() && strpos($currpage, get_option('siteurl')."/listing/")!==false){
		apply_filters( 'page_template', varetreat_listing($template) );
	}
	
}

add_action("init", "varetreat_xml_single_page", 1);


function varetreat_listing($t){
	global $blog_id;
	global $wpdb;

		$tmp = split("/listing/", $_SERVER['REQUEST_URI']);
		$key = split("/", $tmp[1]);
		$id = $key[0];

		get_header();


		if($_GET["f"]!="" && $_GET["f"]!=null){
			$feed_url = "http://www.virginia.org/events/xml/VARetreat/" . $_GET["f"] . ".xml";
		} else {
			if($_COOKIE["xml_file"]!="" && $_COOKIE["xml_file"]!=null){
				$feed_url = $_COOKIE["xml_file"];
			} else {
				$feed_url = "http://www.virginia.org/events/xml/VARetreat/locAttrVARTRT.xml";
			}
		}
		



		$opts = array('http' => array('user_agent' => 'PHP libxml agent'));

		$context = stream_context_create($opts);
		libxml_set_streams_context($context);

		$doc = new DOMDocument();
		$doc = DOMDocument::load($feed_url);

		// $elements = $doc->getElementsByTagName("item"); 
		
		$xpath = new DOMXpath($doc);
		$elements = $xpath->query("item[attrID='" . $id . "'][position() < 2]");
		
		foreach ($elements as $element) {
			include_once($_SERVER["DOCUMENT_ROOT"] . '/wp-content/themes/varetreat/listing.php');
		}
		get_footer();
		exit;
	
}


/* ERIK USE THE FUNCTION BELOW */

function varetreat_xml($url, $num, $textlen){
	$output = "";
	$xml = file_get_html($url);
	$z = 1;
	
	$count = 0;
	
	setcookie("xml_file", $url, time()+3600, "/");
	
	
	foreach($xml->find("item") as $item){
		
		$count++;
		
		if($z==1){ $output .= "<li class=\"odd\">"; } else { $output .= "<li>"; }

		if(trim($item->find("daterange",0)->innertext)!=""){
			$output .= "<p class='date'>" . $item->find("daterange",0)->innertext . "</p>"; 
		}


		$output .= "
						<h3>
							<a href=\"/listing/" . $item->find("attrID",0)->innertext . "/\">" . $item->find("title",0)->innertext . "</a>
						</h3>"; 
						

						
		$output .= "<b>" . $item->find("locality",0)->innertext . "</b><br/>"; 
						
		if($textlen == "" || $textlen == "-1"){
			$output .= strip_tags(html_entity_decode($item->find("description",0)->innertext));
		} else {
			$output .= substr(strip_tags(html_entity_decode($item->find("description",0)->innertext)), 0, intval($textlen)) . "&hellip";
		}
		
						
		$output .= "</li>\n";
		$z*=-1;
		
		if(intval($num)!=-1 && $count > intval($num)){ break; }
		
	}
	
	if($output!=""){ $output = "<ul class=\"zebra\">" . $output . "</ul>"; }
	
	return $output;
	
}

function varetreat_xml_single_feed($atts, $content = null) {
	extract(shortcode_atts(array(
		"url" => ""
	), $atts));

	return varetreat_xml($url, -1, 200);
}
add_shortcode("listings", "varetreat_xml_single_feed");

// split content at the more tag and return an array
function split_content() {
	global $more;
	$more = true;
	$content = preg_split('/<span id="more-\d+"><\/span>/i', get_the_content('more'));
	for($c = 0, $csize = count($content); $c < $csize; $c++) {
		$content[$c] = apply_filters('the_content', $content[$c]);
	}
	return $content;
}


ob_start();

?>