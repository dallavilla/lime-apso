<?php get_header(); ?>
<div id="container">
	<div class="container_12 clearfix">
		<div class="grid_9">
			<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
			<?php query_posts($query_string . "&cat=-3"); ?>
			<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>
			<div class="article">
				<p class="date"><?php the_time('F jS, Y'); ?></p>
				<h2 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				<?php the_content('+ Read More'); ?>
			</div>
			<?php endwhile; ?>
			<?php else : ?>
			<h2>Sorry, there are no news entries at this time.</h2>
			<?php endif; ?>
		</div>
		<?php get_sidebar(); ?>
	</div>
</div>
<?php get_footer(); ?>