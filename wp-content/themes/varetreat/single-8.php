<?php get_header(); ?>
<div id="container">

<div class="search-results">
	
	<table>
		<tr>
			<td>
				<img src="<?php bloginfo('template_directory'); ?>/images/highlight-todo.jpg">
			</td>
			<td>
				<h2 style="margin: 40px 0px 10px 0px">What to Do</h2>
				<div style="padding-right: 150px;"><?php echo category_description(8); ?></div>
			</td>
		</tr>
		<tr>
			<td class="search-categories">
				
				<div>
					<h2>Categories</h2>
				
					<h3><a href="/what-to-do/">See All</a></h3>
					<?php
							$args=array(
								'post_type' => 'post',
								'post_status' => 'publish',
								'posts_per_page' => 20,
								'caller_get_posts'=> 1,
								'category_name' => 'What To Do',
								'orderby' => 'title',
								'order' => 'asc'
							);
					
						    $my_query = null;
						    $my_query = new WP_Query($args);
						    if( $my_query->have_posts() ) {
								while ($my_query->have_posts()) : $my_query->the_post(); ?>
									<h3 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink(); ?>"<?php if(strpos($_SERVER["REQUEST_URI"], strtolower(get_the_title()) )!==false){ print " class='selected'"; } ?>><?php the_title(); ?></a></h3>
								<?php
								endwhile;
							}
							wp_reset_query();
					?>
				</div>
			</td>
			<td>
				
				<div class="search-listings">
					
					<div id="google_map">
						
					</div>
					

					<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>

						<div id="search_results">

							<?php the_content('+ Read More'); ?>

							<div class="paginations"></div>

						</div>
						

						<div name="paginationoptions" style="display: none;">
							<input type="hidden" value="20" name="items_per_page" id="items_per_page" class="numeric"/>
							<input type="hidden" value="10" name="num_display_entries" id="num_display_entries" class="numeric"/>
							<input type="hidden" value="2" name="num_edge_entries" id="num_edge_entries" class="numeric"/>
							<input type="hidden" value="Prev" name="prev_text" id="prev_text"/>
							<input type="hidden" value="Next" name="next_text" id="next_text"/>
						</div>


					<?php endwhile; ?>
					<?php else : ?>
						<h2>Sorry, the entry you are looking for is not here. <a href="<?php echo get_option('url'); ?>">Return Home</a>.</h2>
					<?php endif; ?>

				</div>
				
			</td>
		</tr>
	</table>
	
	
	<script type="text/javascript" charset="utf-8">

		$(document).ready(function() {
			if( $(".zebra li").length < 21 ) { $(".paginations").css("display", "none"); }
		});
		
	</script>
	
	
</div>
	
</div>
<?php get_footer(); ?>   