<?php get_header(); ?>
<div id="container">
	<div class="container_12 clearfix">
		<div class="grid_9">
			<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
			<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>
				<div class="section">
					<h2><?php the_title(); ?></h2>
					<?php the_content('+ Read More'); ?>
				</div>
			<?php endwhile; ?>
			<?php else : ?>
				<h2>Sorry, the page you are looking for is not here. <a href="<?php echo get_option('url'); ?>">Return Home</a>.</h2>
			<?php endif; ?>
		</div>
		<?php get_sidebar(); ?>
	</div>
</div>
<?php get_footer(); ?>