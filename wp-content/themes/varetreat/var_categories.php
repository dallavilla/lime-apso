<?php

$events_city=",victoria,petersburg,alberta,clarksville,south boston,south hill,farmville,alton,brookneal,buckingham,appomattox,charlotte court house,powhatan,cumberland,chase city,la crosse,blackstone";

$events_locality=",lunenburg,petersburg (city),brunswick,mecklenburg,halifax,prince edward,charlotte,buckingham,appomattox,powhatan,cumberland,nottoway";

$lodging_city=",alton,amelia,appomattox,blackstone,boydton,bracey,buckingham,buffalo junction,charlotte court house,chase city,clarksville,concord,cumberland,farmville,gasburg,gladstone,green bay,halifax,jetersville,kenbridge,keysville,la crosse,lawrenceville,petersburg,powhatan,prospect,randolph,red oak,rice,scottsburg,scottsville,south boston,south hill,victoria,virgilina,clover,sutherlin";

$lodging_locality=",halifax,amelia,appomattox,dinwiddie,nottoway,mecklenburg,buckingham,charlotte,cumberland,prince edward,brunswick,lunenburg,petersburg (city),powhatan";

$lodging_type=",accessible,bed &amp; breakfast / inns,pet friendly,hotels &amp; motels,camping,cottages &amp; cabins,vacation homes &amp; rentals,lodges &amp; guest ranches,green lodging,group friendly,resorts";

$dining_city=",alberta,amelia,appomattox,blackstone,boydton,bracey,chase city,clarksville,cumberland,farmville,gasburg,halifax,hampton,keysville,lawrenceville,meherrin,petersburg,powhatan,south boston,south hill,victoria";

$dining_locality=",brunswick,amelia,appomattox,nottoway,mecklenburg,cumberland,prince edward,halifax,charlotte,petersburg (city),dinwiddie,powhatan,lunenburg";

$shopping_city=",alberta,appomattox,blackstone,bracey,buckingham,chase city,clarksville,crewe,dillwyn,dinwiddie,farmville,gasburg,halifax,kenbridge,keysville,la crosse,lawrenceville,petersburg,powhatan,south boston,south hill,victoria,virgilina";

$shopping_locality=",brunswick,appomattox,nottoway,mecklenburg,buckingham,dinwiddie,prince edward,halifax,lunenburg,charlotte,petersburg (city),powhatan";

$attr_city=",alberta,alton,appomattox,blackstone,boydton,bracey,brookneal,buckingham,buffalo junction,burkeville,charlotte county,charlotte court house,chase city,clarksville,crewe,cumberland,cumberland county,dinwiddie,drakes branch,farmville,gasburg,green bay,halifax,hampden-sydney,keysville,la crosse,lawrenceville,lunenburg,mckenney,nathalie,nottoway,pamplin city,petersburg,powhatan,prince george,randolph,rawlings,red oak,rice,saxe,scottsburg,skipwith,south boston,south hill,spout spring,victoria";

$attr_locality=",brunswick,halifax,appomattox,nottoway,mecklenburg,charlotte,buckingham,cumberland,dinwiddie,prince edward,lunenburg,petersburg (city),powhatan,amelia";

?>