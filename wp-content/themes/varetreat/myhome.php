<?php
/*
Template Name: Home
*/
?>

<?php get_header(); ?>
<div id="homepaper">
<div id="homecontainer">
	<div class="container_12">
		<div class="grid_7 hometext">
			<p><strong>Ours is a land of rolling hills and winding waters.</strong> A countryside with stories to tell and small towns keeping watch over a treasured way of life. There&#x27;s quiet abundance here, riches born of knowing what matters most. Living close to nature. Respect for those who came before us. Being in this together. And knowing another adventure is always waiting just beyond our back gate.</p>
			<h3>Let&#x27;s go find an Adventure!</h3>
		</div>
		<div>
			<div class="callToAction actionGreen">
				<div class="greenbar"></div>
				<h2>Experience History</h2>
				<p><img src="<?php bloginfo('template_directory'); ?>/images/robert_e_lee.jpg" alt="Experience History"/></p>
				<p><a href="<?php bloginfo('url'); ?>/category/adventures/history-adventures/">Click for more info</a></p>
			</div>
			<div class="callToAction actionYellow">
				<div class="yellowbar"></div>
				<h2>Get Outdoors</h2>
				<p><img src="<?php bloginfo('template_directory'); ?>/images/outdoors.jpg" alt="Get Outdoors"/></p>
				<p><a href="<?php bloginfo('url'); ?>/category/adventures/outdoor-adventures/">Click for more info</a></p>
			</div>
		</div>
	</div>
	<div id="footerhome">
		<div class="container_12">
			<div class="grid_12">
				<p class="auxnav"><a href="<?php bloginfo('url'); ?>">Home</a> | <a href="<?php echo get_category_link(3); ?>">About the Region</a> | <a href="<?php echo get_category_link(8); ?>">What to Do</a> | <a href="<?php echo get_category_link(9); ?>">Where to Stay</a> | <a href="<?php echo get_category_link(6); ?>">History Adventures</a> | <a href="<?php echo get_category_link(7); ?>">Outdoor Adventures</a> | <a href="<?php echo get_page_link(10); ?>">Contact Us</a></p>
				<span style="float:left; padding-bottom:18px; padding-right: 22px;"><?php get_search_form(); ?></span>
				<p class="clearleft">Copyright &copy; <?php echo date('Y'); ?> <?php bloginfo('name');?></p>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
</div>
</body>
</html>