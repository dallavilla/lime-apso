<?php get_header(); ?>
<div id="container">
	<div class="container_12 clearfix">
		<div id="adventurehero">
			<a href="<?php echo get_category_link(6); ?>" title="Experience History"><img src="<?php bloginfo('template_directory'); ?>/images/experiencehistoryhero.jpg" alt="Experience History" /></a><a href="<?php echo get_category_link(7); ?>" title="Get Outdoors"><img src="<?php bloginfo('template_directory'); ?>/images/getoutdoorshero.jpg" alt="Get Outdoors" /></a>
		</div>
		<div class="grid_9">
			<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
			<h2>Where History and Outdoors Meet, There's Always Adventure</h2>
			<p><?php echo category_description(4); ?></p>
		</div>
		<div class="grid_3">
			<p>&nbsp;</p>
			<p style="text-align: center;">
				<a href="/adventures/history-adventures/civil-rights-in-education-heritage-trail/"><img src="<?php bloginfo('template_directory'); ?>/images/crieht.jpg" /> </a>
				<a href="http://www.civilwartraveler.com/EAST/VA/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/cwt.jpg" /></a>
			</p>
		</div>
		<hr />
		<div class="grid_6">
			<?php
			$posts = query_posts($query_string . 
			'&orderby=title&order=asc&posts_per_page=3&cat=6');
			// here comes The Loop!
			if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="excerpt">
				<?php if ( has_post_thumbnail()) : ?>
				<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
				<?php the_post_thumbnail('large', array('class' => 'shadow floatleft')); ?>
				</a>
				<?php endif; ?>
				<h3 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink(); ?>">
					<?php the_title(); ?>
					</a></h3>
				<?php the_excerpt(''); ?>
				<p class="url"><a class="more" href="<?php the_permalink(); ?>">click for more info</a></p>
			</div>
			<?php endwhile; ?>
			<?php else : ?>
			<h2>Sorry, there are no news entries at this time.</h2>
			<?php endif; ?>
			<p><a class="more" href="<?php echo get_category_link(6); ?>">click for more history adventures</a></p>
		</div>
		<div class="grid_6">
			<?php
			$posts = query_posts($query_string . 
			'&orderby=title&order=asc&posts_per_page=3&cat=7');
			// here comes The Loop!
			if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="excerpt">
				<?php if ( has_post_thumbnail()) : ?>
				<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
				<?php the_post_thumbnail('large', array('class' => 'shadow floatleft')); ?>
				</a>
				<?php endif; ?>
				<h3 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink(); ?>">
					<?php the_title(); ?>
					</a></h3>
				<?php the_excerpt(''); ?>
				<p class="url"><a class="more" href="<?php the_permalink(); ?>">click for more info</a></p>
			</div>
			<?php endwhile; ?>
			<?php else : ?>
			<h2>Sorry, there are no news entries at this time.</h2>
			<?php endif; ?>
			<p class="clearleft"><a class="more" href="<?php echo get_category_link(7); ?>">click for more outdoor adventures</a></p>
		</div>
	</div>
</div>
<?php get_footer(); ?>