<?php get_header(); ?>
<div id="container">
	<div class="container_12 clearfix">
		<img src="<?php bloginfo('template_directory'); ?>/images/historyhero.jpg" class="hero" alt="Experience History" />
		<div class="grid_9">
			<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
			<h2>Stand Where History Was Made</h2>
			<p><?php echo category_description(6); ?></p>
		</div>
		<hr />
		<?php
		 $posts = query_posts($query_string . 
		'&orderby=title&order=asc&posts_per_page=-1'); 
		// here comes The Loop!
		if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div class="grid_6 postbox" style="margin-bottom: 20px">
			<?php if ( has_post_thumbnail()) : ?>
				<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
				<?php the_post_thumbnail('large', array('class' => 'shadow floatleft')); ?>
				</a>
			<?php endif; ?>
			<h3 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink(); ?>" target="_blank"><?php the_title(); ?></a></h3>
			<?php the_excerpt(''); ?>
			<p class="url"><a class="more" href="<?php the_permalink(); ?>" target="_blank">click for more info</a></p>
		</div>
		<?php endwhile; ?>
		<?php else : ?>
		<div class="grid_12"><h2>Sorry, there are no news entries at this time.</h2></div>
		<?php endif; ?>
		<hr />
		<div class="grid_6">
			<h3>Historic Attractions</h3>
			<p>Small towns throughout Virginia's Retreat bear witness to centuries of American life, each with its own array of historic courthouses, homes and buildings. In addition, many authentic sites throughout the region have been preserved or restored to give you a true sense of the conditions and atmosphere of long ago Virginia. Among the most popular attractions are <a href="/listing/60233/?f=locAttrVARTRT">Appomattox Court House and National Historic Park</a> including the house where Generals Robert E. Lee and Ulysses S. Grant negotiated the terms of surrender to end the Civil War. <a href="/listing/11038/?f=locAttrVARTRT">Pamplin Historical Park and The National Museum of the Civil War Soldier</a> provide both an experiential outdoor encampment and a wealth of artifacts and insight into daily life, both on the battlefield and the homefront. <a href="/listing/10529/?f=locAttrVARTRT">Red Hill and the Patrick Henry National Memorial</a> give a view of the tranquility of post-Revolutionary War life for this famous patriot, complete with restored gardens.</p>
			<p><a href="/listing/20200/?f=locAttrVARTRT">The Robert Russa Moton Museum</a> is housed on the site of a 1951 student body walkout that set in motion the nation's move forward for Civil Rights in education. The museum commemorates this event along with scores of other courageous efforts and is part of the groundbreaking Civil Rights in Education Heritage Trail.</p>
			<p><a class="more" href="<?php echo get_category_link(8); ?>">click for more info on what to do</a></p>
		</div>
		<div class="grid_6">
			<h3>Upcoming Events</h3>
			<div id="eventspromo">
				<?php print varetreat_xml('http://www.virginia.org/events/xml/VARetreat/locEventsVARTRT.xml', 3, 200 ); ?>
				<p><a class="more" href="/category/what-to-do">click for all event listings</a></p>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>