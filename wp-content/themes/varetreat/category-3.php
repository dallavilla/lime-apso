<?php get_header(); ?>
<div id="container">
	<div class="container_12 clearfix">
		<img src="<?php bloginfo('template_directory'); ?>/images/aboutTheRegion.jpg" class="hero" alt="About the Region"/>
		<div class="grid_9">
			<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
			<h2>Welcome to Virginia's Retreat</h2>
			<p><?php echo category_description(3); ?></p>
			<h2>History of the Consortium</h2>
			<p><?php echo category_description(10); ?></p>
		</div>
		<div class="grid_3">
			<p>&nbsp;</p>
			<p style="text-align: center;">
				<a href="/adventures/history-adventures/civil-rights-in-education-heritage-trail/"><img src="<?php bloginfo('template_directory'); ?>/images/crieht.jpg" /> </a>
				<a href="http://www.civilwartraveler.com/EAST/VA/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/cwt.jpg" /></a>
			</p>
		</div>
		<hr />
		<?php
		 $posts = query_posts($query_string . 
		'&orderby=title&order=asc&posts_per_page=-1'); 
		// here comes The Loop!
		if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div class="grid_6 postbox" style="margin-bottom: 20px">
			<?php if ( has_post_thumbnail()) : ?>
				<a href="<?php echo get_post_meta($post->ID, 'link', true) ?>" title="<?php the_title_attribute(); ?>" target="_blank"><?php the_post_thumbnail('large', array('class' => 'shadow floatleft')); ?></a>
			<?php endif; ?>
			<h3 id="post-<?php the_ID(); ?>"><a href="<?php echo get_post_meta($post->ID, 'link', true) ?>" target="_blank"><?php the_title(); ?></a></h3>
			<?php the_content(''); ?>
			<?php if ( get_post_meta($post->ID, 'link', true) ) : ?>
				<p class="url"><a class="more" href="<?php echo get_post_meta($post->ID, 'link', true) ?>" target="_blank">visit our website</a></p>
			<?php endif; ?>
		</div>
		<?php endwhile; ?>
		<?php else : ?>
		<div class="grid_12"><h2>Sorry, there are no news entries at this time.</h2></div>
		<?php endif; ?>
		<hr />
		
		<?php
		$post_id = 539;
		$queried_post = get_post($post_id);
		$title = $queried_post->post_title;
		echo "<div class=\"grid_9\"><h2>" . $title . "</h2></div>";
		echo "<p>" . $queried_post->post_content . "</p>";
		?>
		
	</div>
</div>
<?php get_footer(); ?>